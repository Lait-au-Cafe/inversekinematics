﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InverseKinematics : MonoBehaviour {
    public int count = 3;
    List<Transform> arms;
    List<float> angles;
    Transform head;
    Transform target;


	// Use this for initialization
	void Start () {
        // collect joints
        arms = new List<Transform>();
        angles = new List<float>();
        var tmp = new Stack<Transform>();

        //var trans = transform;
        //while(trans.childCount != 0)
        for(int i = 1; i <= count; i++)
        {
            //trans = trans.GetChild(0);
            var arm = GameObject.CreatePrimitive(PrimitiveType.Cube).transform;
            arm.localScale = new Vector3(1, 1, 6);
            arm.position = new Vector3(0, 0, 5f * i);

            tmp.Push(arm);
            //if(trans.tag == "Joint")
            //{
            //    tmp.Push(trans);
            //}
        }
        while(tmp.Count != 0)
        {
            arms.Add(tmp.Pop());
            angles.Add(0.0f);
        }

        // find head
        //head = GameObject.FindGameObjectWithTag("Head").transform;
        head = new GameObject().transform;
        head.position = new Vector3(0, 0, 5f * count + 3f);
        head.SetParent(arms[0]);
        if (head == null)
        {
            Debug.LogError("Cannot find head. ");
            return;
        }

        // find target
        target = GameObject.FindGameObjectWithTag("Target").transform;
        if(target == null)
        {
            Debug.LogError("Cannot find target. ");
            return;
        }

        //Debug.Log("Stack : " + tmp.Count);
        //Debug.Log("List : " + arms.Count);
    }

    float timeElapsed = 0;
    int cnt = 1;

	// Update is called once per frame
	void Update () {
        timeElapsed += Time.deltaTime;
        if(timeElapsed < -1)
        {
            return;
        }
        timeElapsed = 0;

        for (int k = 0; k < 500; k++)
        {

            for (int i = 0; i < count; i++)
            {
                var arm = arms[i];
                var axis4 = arm.localToWorldMatrix * new Vector4(0, 0, -0.4f, 1);
                var axis = new Vector3(axis4.x, axis4.y, axis4.z);

                var dest = (target.position - axis).normalized;
                var relative = (head.position - axis).normalized;

                var sin = Vector3.Cross(relative, dest).x;
                var arg = 180 * Mathf.Asin(sin) / Mathf.PI;
                if (Vector3.Dot(relative, dest) < 0)
                {
                    arg = 180 - arg;
                }

                var angle = Mathf.Min(80, Mathf.Max(-80, angles[i] + arg));
                arg = angle - angles[i];
                angles[i] = angle;

                //Debug.Log(axis);
                //Debug.Log(arg);
                for (int j = 0; j <= i; j++)
                {
                    arms[j].RotateAround(axis, new Vector3(1, 0, 0), arg);
                }

                //Debug.Log("Count : " + cnt);
                //cnt++;
            }

        }
	}
}
